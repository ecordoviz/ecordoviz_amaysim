package amaysim.shopping.model;

/**
 * This class represents the product available for shopping.
 *
 * @author ECordoviz
 */
public class Product {

    // Product Code
    private String productCode;
    // Product Name
    private String productName;
    // Price
    private Double price;

    /**
     * Product constructor
     *
     * @param code Product Code
     * @param name Product Name
     * @param price Price
     */
    public Product(String code, String name, Double price) {
        setProductCode(code);
        setProductName(name);
        setPrice(price);
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Double price) {
        this.price = price;
    }
    
}
