/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amaysim.shopping.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Core-enabler
 */
public class ShoppingCart {

    private Map<String, OrderedProduct> items;

    private int numberOfItems;

    private double subTotalPrice;

    private double total;

    private String promoCode;

    public ShoppingCart() {
        setItems(new HashMap<String, OrderedProduct>());
        setNumberOfItems(getItems().size());
        setSubTotalPrice(0.0);
        setTotal(0.0);
        setPromoCode("");
    }

    public void add(OrderedProduct product) {
        
        if (getItems().containsKey(product.getProduct().getProductCode())) {
            OrderedProduct op =  getItems().get(product.getProduct().getProductCode());
            int qt = op.getQuantity();
            
            op.setQuantity(qt + product.getQuantity());
        } else {
            getItems().put(product.getProduct().getProductCode(), product);
        }
        
    }

    public void add(OrderedProduct product, String promoCode) {
        add(product);

        setPromoCode(promoCode);
    }

    /**
     * @return the promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * @param promoCode the promoCode to set
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    /**
     * @return the items
     */
    public Map<String, OrderedProduct> getItems() {
        return items;
    }

    /**
     * @param productsOrdered the items to set
     */
    public void setItems(Map<String, OrderedProduct> productsOrdered) {
        this.items = productsOrdered;
    }

    /**
     * @return the numberOfItems
     */
    public int getNumberOfItems() {
        if (getItems() != null && getItems().isEmpty()) {
            setNumberOfItems(getItems().size());
        }

        return 0;
    }

    private void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    /**
     * @return the subTotalPrice
     */
    public double getSubTotalPrice() {
        return subTotalPrice;
    }

    /**
     * @param subTotalPrice the subTotalPrice to set
     */
    public void setSubTotalPrice(double subTotalPrice) {
        this.subTotalPrice = subTotalPrice;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param totalPrice the total to set
     */
    public void setTotal(double totalPrice) {
        this.total = totalPrice;
    }

}
