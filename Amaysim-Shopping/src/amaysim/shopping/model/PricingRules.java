/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amaysim.shopping.model;

import amaysim.shopping.enums.ItemCountConditionEnum;
import amaysim.shopping.enums.PriceApplicationEnum;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Core-enabler
 */
public class PricingRules {

    private Product item;

    private String productCode;

    private Double totalPrice;

    private Double discountedPrice;

    private PriceApplicationEnum priceApplication;

    private List<Product> freebies;

    private int requiredItemCount;

    private ItemCountConditionEnum itemCountCondition;
    
    public PricingRules(Product item, Double totalPrice,
            Double discountedPrice, PriceApplicationEnum priceApplication, int requiredItemCount,
            ItemCountConditionEnum itemCountCondition) {
        setItem(item);
        setProductCode(item.getProductCode());
        setTotalPrice(totalPrice);
        setDiscountedPrice(discountedPrice);
        setPriceApplication(priceApplication);
        setFreebies(new ArrayList<Product>());
        setRequiredItemCount(requiredItemCount);
        setItemCountCondition(itemCountCondition);
    }

    public PricingRules(Product item, Double totalPrice,
            Double discountedPrice, PriceApplicationEnum priceApplication,
            List<Product> freebies,
            int requiredItemCount, ItemCountConditionEnum itemCountCondition) {
        setItem(item);
        setProductCode(item.getProductCode());
        setTotalPrice(totalPrice);
        setDiscountedPrice(discountedPrice);
        setPriceApplication(priceApplication);
        setFreebies(freebies);
        setRequiredItemCount(requiredItemCount);
        setItemCountCondition(itemCountCondition);
    }

    /**
     * @return the totalPrice
     */
    public Double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the discountedPrice
     */
    public Double getDiscountedPrice() {
        return discountedPrice;
    }

    /**
     * @param discountedPrice the discountedPrice to set
     */
    public void setDiscountedPrice(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    /**
     * @return the freebies
     */
    public List<Product> getFreebies() {
        return freebies;
    }

    /**
     * @param freebies the freebies to set
     */
    public void setFreebies(List<Product> freebies) {
        this.freebies = freebies;
    }

    /**
     * @return the requiredItemCount
     */
    public int getRequiredItemCount() {
        return requiredItemCount;
    }

    /**
     * @param requiredItemCount the requiredItemCount to set
     */
    public void setRequiredItemCount(int requiredItemCount) {
        this.requiredItemCount = requiredItemCount;
    }

    /**
     * @return the itemCountCondition
     */
    public ItemCountConditionEnum getItemCountCondition() {
        return itemCountCondition;
    }

    /**
     * @param itemCountCondition the itemCountCondition to set
     */
    public void setItemCountCondition(ItemCountConditionEnum itemCountCondition) {
        this.itemCountCondition = itemCountCondition;
    }

    /**
     * @return the item
     */
    public Product getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Product item) {
        this.item = item;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the priceApplication
     */
    public PriceApplicationEnum getPriceApplication() {
        return priceApplication;
    }

    /**
     * @param priceApplication the priceApplication to set
     */
    public void setPriceApplication(PriceApplicationEnum priceApplication) {
        this.priceApplication = priceApplication;
    }

    /**
     * @return the hasFreebies
     */
    public boolean hasFreebies() {
        return (getFreebies()!=null && !getFreebies().isEmpty());
    }

}
