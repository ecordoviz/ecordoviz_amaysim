/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amaysim.shopping.model;

import amaysim.shopping.enums.DiscountType;

/**
 *
 * @author Core-enabler
 */
public class PromoCode {
    
    private String promoCode;
    
    private float discount;
    
    private DiscountType discountType;
    
    public PromoCode (String promoCode, float discount, DiscountType discountType ) {
        setPromoCode(promoCode);
        setDiscount(discount);
        setDiscountType(discountType);
    }

    /**
     * @return the promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * @param promoCode the promoCode to set
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    /**
     * @return the discount
     */
    public float getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(float discount) {
        this.discount = discount;
    }

    /**
     * @return the discountType
     */
    public DiscountType getDiscountType() {
        return discountType;
    }

    /**
     * @param discountType the discountType to set
     */
    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }
    
    
}
