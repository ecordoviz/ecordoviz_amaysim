package amaysim.shopping.factory;

import amaysim.shopping.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the product factory that returns the list of available
 * products for shopping.
 *
 * @author ECordoviz
 */
public class ProductFactory {
    
    private static final ProductFactory INSTANCE =  new ProductFactory();
    
    private List<Product> availableProducts;
    
    private ProductFactory() {
        setAvailableProducts(new ArrayList<Product>());
        
        Product product = new Product("ult_small", "Unlimited 1GB", 24.90d);
        getAvailableProducts().add(product);
        
        product = new Product("ult_medium", "Unlimited 2GB", 29.90d);
        getAvailableProducts().add(product);
        
        product = new Product("ult_large", "Unlimited 5GB", 44.90d);
        getAvailableProducts().add(product);
        
        
        product = new Product("1gb", "1 GB Data-pack", 9.90d);
        getAvailableProducts().add(product);
    }
    
    public Product getProduct(String code) {
        for (Product p : getAvailableProducts()) {
            if (p.getProductCode().equals(code)) {
                return p;
            }
        }
        
        return null;
    }

    /**
     * Returns the only instance of the Product Factory
     *
     * @return
     */
    public static ProductFactory getInstance(){
        if (INSTANCE == null)
            return new ProductFactory();
        
        return INSTANCE;
    }
    
    /**
     * @return the availableProducts
     */
    public List<Product> getAvailableProducts() {
        return availableProducts;
    }

    /**
     * @param availableProducts the availableProducts to set
     */
    public void setAvailableProducts(List<Product> availableProducts) {
        this.availableProducts = availableProducts;
    }
}
