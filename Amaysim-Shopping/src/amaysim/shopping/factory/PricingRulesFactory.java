/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amaysim.shopping.factory;

import amaysim.shopping.enums.ItemCountConditionEnum;
import amaysim.shopping.enums.PriceApplicationEnum;
import amaysim.shopping.model.PricingRules;
import amaysim.shopping.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Core-enabler
 */
public class PricingRulesFactory {

    private static final PricingRulesFactory INSTANCE = new PricingRulesFactory();

    private ProductFactory productFactory = ProductFactory.getInstance();

    private List<PricingRules> availablePricingRules;

    private PricingRulesFactory() {

        setAvailablePricingRules(new ArrayList<PricingRules>());

        Product item = productFactory.getProduct("ult_small");
        PricingRules pricingRule = new PricingRules(item, item.getPrice(), item.getPrice() * 2,
                PriceApplicationEnum.TOTAL, 3, ItemCountConditionEnum.EQUAL);
        getAvailablePricingRules().add(pricingRule);

        item = productFactory.getProduct("ult_large");
        pricingRule = new PricingRules(item, item.getPrice(), 39.90d, PriceApplicationEnum.FOR_EACH,
                3, ItemCountConditionEnum.MORE_THAN);
        getAvailablePricingRules().add(pricingRule);

        item = productFactory.getProduct("ult_medium");
        List<Product> freebies = new ArrayList<Product>();
        freebies.add(productFactory.getProduct("1gb"));
        pricingRule = new PricingRules(item, item.getPrice(), item.getPrice(), PriceApplicationEnum.FOR_EACH,
                freebies, 1, ItemCountConditionEnum.EQUAL);
        getAvailablePricingRules().add(pricingRule);
    }

    public void addNewPriceRule(PricingRules pricingRule) {
        getAvailablePricingRules().add(pricingRule);
    }

    public PricingRules getPricingRule(String productCode) {
        for (PricingRules pricingRule : getAvailablePricingRules()) {
            if (pricingRule.getProductCode().equals(productCode)) {
                return pricingRule;
            }
        }
        return null;
    }

    /**
     * @return the INSTANCE
     */
    public static PricingRulesFactory getINSTANCE() {
        if (INSTANCE == null) {
            return new PricingRulesFactory();
        }

        return INSTANCE;
    }

    /**
     * @return the availablePricingRules
     */
    public List<PricingRules> getAvailablePricingRules() {
        return availablePricingRules;
    }

    /**
     * @param availablePricingRules the availablePricingRules to set
     */
    public void setAvailablePricingRules(List<PricingRules> availablePricingRules) {
        this.availablePricingRules = availablePricingRules;
    }

    /**
     * @return the productFactory
     */
    public ProductFactory getProductFactory() {
        return productFactory;
    }

    /**
     * @param productFactory the productFactory to set
     */
    public void setProductFactory(ProductFactory productFactory) {
        this.productFactory = productFactory;
    }
}
