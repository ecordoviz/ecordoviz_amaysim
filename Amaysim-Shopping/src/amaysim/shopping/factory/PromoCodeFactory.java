/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amaysim.shopping.factory;

import amaysim.shopping.enums.DiscountType;
import amaysim.shopping.model.PromoCode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Core-enabler
 */
public class PromoCodeFactory {
    
    private static final PromoCodeFactory INSTANCE = new PromoCodeFactory();
    
    private List<PromoCode> promoCodes;
    
    private PromoCodeFactory() {
        setPromoCodes(new ArrayList<PromoCode>());
        
        PromoCode promo = new PromoCode("I<3AMAYSIM", 10f, DiscountType.PERCENTAGE);
        getPromoCodes().add(promo);
    }

    public void addPromoCode(PromoCode promo) {
        getPromoCodes().add(promo);
    }
    
    public PromoCode getPromoCode (String promoCode){
        for (PromoCode promo : getPromoCodes()) {
            if (promo.getPromoCode().equals(promoCode)) {
                return promo;
            }
        }
        return null;
    }
    /**
     * @return the INSTANCE
     */
    public static PromoCodeFactory getINSTANCE() {
        if (INSTANCE == null) {
            return new PromoCodeFactory();
        }
        
        return INSTANCE;
    }

    /**
     * @return the promoCodes
     */
    public List<PromoCode> getPromoCodes() {
        return promoCodes;
    }

    /**
     * @param promoCodes the promoCodes to set
     */
    public void setPromoCodes(List<PromoCode> promoCodes) {
        this.promoCodes = promoCodes;
    }
    
}
