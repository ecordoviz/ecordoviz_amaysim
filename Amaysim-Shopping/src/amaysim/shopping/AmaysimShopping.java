package amaysim.shopping;

import amaysim.shopping.enums.DiscountType;
import amaysim.shopping.enums.ItemCountConditionEnum;
import amaysim.shopping.enums.PriceApplicationEnum;
import amaysim.shopping.factory.PricingRulesFactory;
import amaysim.shopping.factory.ProductFactory;
import amaysim.shopping.factory.PromoCodeFactory;
import amaysim.shopping.model.OrderedProduct;
import amaysim.shopping.model.PricingRules;
import amaysim.shopping.model.Product;
import amaysim.shopping.model.PromoCode;
import amaysim.shopping.model.ShoppingCart;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * This class represents the main controller of the application.
 *
 * @author ECordoviz
 */
public class AmaysimShopping {

    private static DecimalFormat DECIMAL_FORMATTER = new DecimalFormat("0.00");

    private static final List<Product> PRODUCTS = ProductFactory.getInstance().getAvailableProducts();

    private static final List<PricingRules> PRICING_RULES = PricingRulesFactory.getINSTANCE().getAvailablePricingRules();

    private static final List<PromoCode> PROMO_CODES = PromoCodeFactory.getINSTANCE().getPromoCodes();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DECIMAL_FORMATTER.setMaximumFractionDigits(2);

        ShoppingCart shoppingCart = new ShoppingCart();
        System.out.println("Welcome to Amaysim. ");

        takeOrder(shoppingCart);

        displayOrder(shoppingCart);

        // Compute order
        System.out.println("\n***************Order Computation***************");

        Map<String, Double> itemSubtotals = new HashMap<String, Double>();
        Map<String, Integer> freebies = new HashMap<String, Integer>();

        for (String pcode : shoppingCart.getItems().keySet()) {
            OrderedProduct op = shoppingCart.getItems().get(pcode);
            Double totalPrice = 0.0;

            // Check if product code has pricing rule
            PricingRules pRule = PricingRulesFactory.getINSTANCE().getPricingRule(pcode);

            if (pRule != null) {
                // price rule applies
                // Check Required Item Count and condition
                int orderCount = op.getQuantity();
                int orderToProcessCount = op.getQuantity();
                if (pRule.getRequiredItemCount() <= orderCount) {

                    while (orderToProcessCount != 0) {
                        if (pRule.getItemCountCondition() == ItemCountConditionEnum.EQUAL) {
                            // Get Required Quantity
                            // Compute price 
                            totalPrice = totalPrice + getDiscountedSubTotal(pRule, totalPrice, pRule.getRequiredItemCount());

                            orderToProcessCount = orderToProcessCount - pRule.getRequiredItemCount();

                            getFreebies(pRule, freebies);
                        }

                        if (pRule.getItemCountCondition() == ItemCountConditionEnum.MORE_THAN) {
                            // Compute price 
                            totalPrice = totalPrice + getDiscountedSubTotal(pRule, totalPrice, orderCount);

                            orderToProcessCount = 0;

                            // Get Freebies
                            getFreebies(pRule, freebies);
                        }
                    }

                } else {
                    totalPrice = op.getQuantity() * op.getProduct().getPrice();
                }

            } else {
                totalPrice = op.getQuantity() * op.getProduct().getPrice();
            }

            System.out.println(op.getQuantity() + " x " + op.getProduct().getProductName() + " : $" + DECIMAL_FORMATTER.format(totalPrice));
            itemSubtotals.put(pcode, totalPrice);
        }

        double total = 0.0;
        for (String pcode : itemSubtotals.keySet()) {
            total = total + itemSubtotals.get(pcode);
        }

        System.out.println("Cart Total : $" + DECIMAL_FORMATTER.format(total));
        
        Double discountedTotal = 0.0;
        if (shoppingCart.getPromoCode()!=null && !shoppingCart.getPromoCode().isEmpty()) {
            PromoCode promo = PromoCodeFactory.getINSTANCE().getPromoCode(shoppingCart.getPromoCode());
            
            if (promo!=null) {
                // Check if discount is in amount or percentage
                if (promo.getDiscountType() == DiscountType.AMOUNT) {
                    discountedTotal = total - promo.getDiscount();
                }
                
                if (promo.getDiscountType() == DiscountType.PERCENTAGE) {
                    double discount = total * (promo.getDiscount()/100);
                    discountedTotal = total - discount;
                }
                
                System.out.println("\n***************'" + promo.getPromoCode() + "' Promo Applied***************");
                System.out.println("Discounted Total : " + DECIMAL_FORMATTER.format(discountedTotal));
            }
                    
        }
        if (!freebies.isEmpty()) { 
            System.out.println("\n***************Freebies***************");
            for (String key : freebies.keySet()) {
                System.out.println(freebies.get(key) + " x " + ProductFactory.getInstance().getProduct(key).getProductName());
            }
        }

    }

    private static void getFreebies(PricingRules pRule, Map<String, Integer> freebies) {
        // Get Freebies
        if (pRule.hasFreebies()) {
            
            for (Product freebie : pRule.getFreebies()) {
                if (freebies.containsKey(freebie.getProductCode())) {
                    int count = freebies.get(freebie.getProductCode()) + 1;
                    
                    freebies.put(freebie.getProductCode(), count);
                } else {
                    freebies.put(freebie.getProductCode(), 1);
                }
            }
            
        }
    }

    private static Double getDiscountedSubTotal(PricingRules pRule, Double totalPrice, int orderCount) {
        if (pRule.getPriceApplication() == PriceApplicationEnum.FOR_EACH) {
            totalPrice = pRule.getDiscountedPrice() * orderCount;
        } else if (pRule.getPriceApplication() == PriceApplicationEnum.TOTAL) {
            totalPrice = pRule.getDiscountedPrice();
        }

        return totalPrice;
    }

    private static void displayOrder(ShoppingCart shoppingCart) {
        // Display shopping cart content
        System.out.println("\n***************Order List***************");

        for (String pcode : shoppingCart.getItems().keySet()) {
            OrderedProduct op = shoppingCart.getItems().get(pcode);
            System.out.println(op.getQuantity() + " x " + op.getProduct().getProductName());
        }

        if (shoppingCart.getPromoCode() != null && !shoppingCart.getPromoCode().isEmpty()) {
            System.out.println("'" + shoppingCart.getPromoCode() + "' Promo Applied");
        }
    }

    private static void takeOrder(ShoppingCart shoppingCart) {
        System.out.println("Please choose an item to order.");
        displayItems();
        Scanner sc = new Scanner(System.in);
        int productId = sc.nextInt();

        if (productId > PRODUCTS.size()) {
            System.out.println("Invalid input.");
        } else {
            Product product = PRODUCTS.get(productId - 1);
            System.out.println("You added " + product.getProductName() + "."
                    + "\nPlease input quantity : ");
            int qt = sc.nextInt();

            System.out.println("Do you want to add promo code? Y/N");
            String addPromo = sc.next();

            if (addPromo.equalsIgnoreCase("y")) {

                System.out.println("Add promo code: ");
                String promoCode = sc.next();
                shoppingCart.add(new OrderedProduct(product, qt), promoCode);

            } else {
                shoppingCart.add(new OrderedProduct(product, qt));
            }

            System.out.println("Do you want to  add more? Y/N");
            String ans = sc.next();

            if (ans.equalsIgnoreCase("y")) {
                takeOrder(shoppingCart);
            }

        }
    }

    private static void displayItems() {
        for (int index = 0; index < PRODUCTS.size(); index++) {
            System.out.println("[" + (index + 1) + "] " + PRODUCTS.get(index).getProductName()
                    + " - $" + DECIMAL_FORMATTER.format(PRODUCTS.get(index).getPrice()));
        }
    }
}
